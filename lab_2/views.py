from django.http import HttpResponse
from django.core import serializers
from django.shortcuts import render
from lab_2.models import Note

# Create index method
def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

# Create xml method
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# Create json method
def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")
