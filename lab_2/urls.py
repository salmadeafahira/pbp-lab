from django.urls import path
from .views import index, json, xml

# Add index, xml, and json path 
urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]