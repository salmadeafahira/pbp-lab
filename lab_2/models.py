from django.db import models

# Create Note model
class Note(models.Model):
    to = models.CharField(max_length=30)
    fromwho = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField()