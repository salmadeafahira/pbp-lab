import 'package:flutter/material.dart';
import '../screens/add_forum.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  // Widget buildListTile(String title, IconData icon, Function tapHandler) {
  //   return ListTile(
  //     leading: Icon(
  //       icon,
  //       size: 26,
  //     ),
  //     title: Text(
  //       title,
  //       style: const TextStyle(
  //         fontFamily: 'RobotoCondensed',
  //         fontSize: 24,
  //         fontWeight: FontWeight.bold,
  //       ),
  //     ),
  //     onTap: tapHandler,
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            // ignore: sized_box_for_whitespace
            Container(
              width: double.infinity,
              height: 90,
              child: const DrawerHeader(
                child: Text(
                  'Swab.In',
                  style: TextStyle(
                    fontWeight: FontWeight.w900,
                    fontSize: 30,
                    color: Colors.white,
                  ),
                ),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(79, 133, 235, 1),
                ),
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.location_on,
                size: 26,
              ),
              title: const Text("Forum Lokasi",
                style: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 18,
                ),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
             ListTile(
              leading: const Icon(
                Icons.forum,
                size: 26,
              ),
              title: const Text("Tambah Lokasi",
                style: TextStyle(
                  fontFamily: 'RobotoCondensed',
                  fontSize: 18,
                ),
              ),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) => const AddForum()
                ));
              },
            ),
          ],
        ),
      );
  }
}