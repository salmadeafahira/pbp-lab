import 'package:flutter/material.dart';
import '../screens/lokasi_homepage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Swab.In',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: const Color.fromRGBO(79, 133, 235, 1),
        fontFamily: 'Raleway',
         textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color:Colors.white),
          headline2: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color:Colors.black),
          headline3: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, color:Colors.black),
          bodyText1: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
          bodyText2: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: const LokasiHomePage(),
    );
  }
}