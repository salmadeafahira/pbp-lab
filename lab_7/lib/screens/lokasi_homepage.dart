import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class LokasiHomePage extends StatefulWidget {
  const LokasiHomePage({Key? key}) : super(key: key);

  @override
  _LokasiHomePageState createState() => _LokasiHomePageState();
}

class _LokasiHomePageState extends State<LokasiHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          title: Text("Swab.In",
          style: Theme.of(context).textTheme.headline1,
          )
        ),
        body: Container(
          margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
          padding: const EdgeInsets.all(18.0),
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  buildCard(),
                  Container( 
                    margin: const EdgeInsets.fromLTRB(20, 50, 20, 10),
                    child: const Align(
                      alignment: Alignment.centerLeft,
                      child: Text('Forum Diskusi', 
                      style: TextStyle(fontSize: 25, fontWeight:FontWeight.bold),
                      textAlign: TextAlign.left,))),
                    buildForum(),
                    buildForum(),
                ],
              ),
            ),
        ),
          drawer: const MainDrawer(),
    );
  }

  Card buildCard() {
      var cardImage = const NetworkImage('https://images.unsplash.com/photo-1601055283742-8b27e81b5553?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bG9rYXNpJTIwc3dhYnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60');
    return Card(
        elevation: 4.0,
        child: Column(
          children: [
            ListTile(
              title: Text('Yos Sudarso, Sunter',
                       style: Theme.of(context).textTheme.headline2,
              ),
              subtitle: Text('(Setelah Altira Business Park)',
                      style: Theme.of(context).textTheme.headline3,
              ),
            ),
            SizedBox(
              height: 200.0,
              child: Ink.image(
                image: cardImage,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16.0),
              alignment: Alignment.centerLeft,
              child: Text(
                      'Jam operasional: Senin-Minggu (08.00-19.00)\nAlamat: Jl. Yos Sudarso Sunter Jaya, Tanjung Priok Jakarta Utara, DKI Jakarta – 14360',
                      style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          ],
        )
      );    
  }

  Card buildForum() {
      var cardImage = const NetworkImage('https://images.unsplash.com/photo-1618229745753-56040f55d7b1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80');
    return Card(
        elevation: 2.0,
        child: Column(
          children: [
            // ignore: sized_box_for_whitespace
            Container(
              height: 180.0,
              child: Ink.image(
                image: cardImage,
                fit: BoxFit.cover,
              ),
            ),
            ListTile(
              title: Text('Review swab di Yos Sudarso',
                       style: Theme.of(context).textTheme.headline2,
              ),
              subtitle: Text('Penulis: Dea',
                      style: Theme.of(context).textTheme.headline3,
              ),
            ),
            Container(
              padding: const EdgeInsets.all(16.0),
              alignment: Alignment.centerLeft,
              child: Text(
                      'Layanan swab test disini memuaskan dan hasilnya keluar dalam 24 jam',
                      style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
             ButtonBar(
              children: [
                TextButton(
                  child: const Text('READ MORE', style: TextStyle(fontWeight: FontWeight.w600)),
                  onPressed: () {},
                )
              ],
            )
          ],
        ));    
  }
}