import 'package:flutter/material.dart';

class AddForum extends StatefulWidget {

  @override
  State<AddForum> createState() => _LokasiHomePageState();
}

class _LokasiHomePageState extends State<AddForum> {
  int counter = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('Tambah Forum',
        style: Theme.of(context).textTheme.headline1,
        )
      ),
      body: Align( alignment: Alignment.center, 
        child:Text('Berhasil ditambahkan $counter forum',
          style: Theme.of(context).textTheme.bodyText2,
          ),
        ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () {
          setState(() {
          });
          counter++;
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}