from django.contrib import admin

# register Friend model 
from .models import Friend
admin.site.register(Friend)