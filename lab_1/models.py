from django.db import models

# Create Friend model that contains name, npm, and DOB attribute
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    dob = models.DateField()