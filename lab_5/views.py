from django.shortcuts import render
from lab_2.models import Note

# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id):
    context = {}
    context["data"] = Note.objects.get(id = id)  
    return render(request, "lab5_index.html", context)
